import { Repository, EntityRepository } from "typeorm"
import { Product } from "./product.entity";
import { CreateProductDto } from "./dto/create.dto";
import { Cart } from "src/cart/cart.entity";
import { CartStatus } from "src/cart/cart-status.enum";

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  async createProduct(createProductDto: CreateProductDto): Promise<Product> {
    const { name, image } = createProductDto
    const product = new Product()
    product.name = name
    product.image = image
    await product.save()
    return product
  }

  async bulkCreateProduct(dtos: CreateProductDto[]): Promise<Product[]> {
    const products = dtos.map(({ name, image }) => {
      const product = new Product()
      product.name = name
      product.image = image
      return product
    })
    await this.save(products)
    return products
  }

  async getRandomProduct(cartId: number): Promise<Product> {
    const cart = cartId && await Cart.findOne({ status: CartStatus.OPEN, id: cartId }, { relations: ['products'] })
    const productsIds = cart ? cart.products.map(p => p.id) : []
    const query = this.createQueryBuilder('product')
    query.orderBy('RANDOM()')
    if(productsIds.length) query.andWhere('NOT product.id IN (:...ids)', { ids: productsIds })
    const product = await query.getOne()
    return product
  }
}