import { Controller, UseGuards, Post, Body, ValidationPipe, Get, Param, ParseIntPipe, Put, Delete, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create.dto';
import { Product } from './product.entity';

@Controller('products')
@UseGuards(AuthGuard('jwt'))
export class ProductController {
  constructor(
    private productService: ProductService,
  ) {}
  
  @Get()
  getAllProducts(): Promise<Product[]> {
    return this.productService.getAllProducts()
  }

  @Get('/random')
  getRandomProduct(
    @Query('cartId') cartId: number
  ): Promise<Product> {
    return this.productService.getRandomProduct(cartId)
  }

  @Get('/:id')
  getProductById(
    @Param('id', ParseIntPipe) id: number
  ): Promise<Product> {
    return this.productService.getProductById(id)
  }

  @Post()
  createProduct(
    @Body(ValidationPipe) createProductDto: CreateProductDto
  ): Promise<Product> {
    return this.productService.createProduct(createProductDto)
  }

  @Post('/bulk')
  bulkCreateProducts(
    @Body('products', ValidationPipe) products: CreateProductDto[]
  ): Promise<Product[]> {
    return this.productService.bulkCreateProducts(products)
  }

  @Put('/:id')
  updateProduct(
    @Param('id', ParseIntPipe) id: number,
    @Body(ValidationPipe) updateProductDto: CreateProductDto
  ): Promise<Product> {
    return this.productService.updateProduct(id, updateProductDto)
  }

  @Delete('/:id')
  deleteProduct(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Product> {
    return this.productService.deleteProduct(id)
  }
}
