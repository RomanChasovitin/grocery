import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductRepository } from './product.repository';
import { CreateProductDto } from './dto/create.dto';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductRepository)
    private productRepository: ProductRepository,
  ) {}
  
  createProduct(createProductDto: CreateProductDto): Promise<Product> {
    return this.productRepository.createProduct(createProductDto)
  }

  bulkCreateProducts(dtos: CreateProductDto[]): Promise<Product[]> {
    return this.productRepository.bulkCreateProduct(dtos)
  }

  getAllProducts(): Promise<Product[]> {
    return this.productRepository.find()
  }

  getRandomProduct(cardId: number): Promise<Product> {
    return this.productRepository.getRandomProduct(cardId)
  }

  async getProductById(id: number): Promise<Product> {
    const product = await this.productRepository.findOne({ id })
    if (!product) throw new NotFoundException('Product not found')
    return product
  }

  async updateProduct(id: number, updateProductDto: CreateProductDto): Promise<Product> {
    const { name, image } = updateProductDto
    const product = await this.getProductById(id)
    product.name = name
    product.image = image
    await product.save()
    return product
  }

  async deleteProduct(id: number): Promise<Product> {
    const product = await this.getProductById(id)
    return await this.productRepository.remove(product)
  }
}
