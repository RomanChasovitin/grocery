import { IsString, MinLength, MaxLength, IsNotEmpty } from "class-validator";

export class CreateProductDto {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  name: string;

  @IsNotEmpty()
  image: string;
}