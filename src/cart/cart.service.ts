import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CartRepository } from './cart.repository';
import { Cart } from './cart.entity';
import { User } from 'src/auth/user.entity';
import { CartStatus } from './cart-status.enum';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(CartRepository)
    private cartRepository: CartRepository,
  ) {}
  
  getAllCarts(user: User): Promise<Cart[]> {
    return this.cartRepository.find({ where: { user }, relations: ['products'] })
  }

  async getCartById(id: number, user: User): Promise<Cart> {
    const cart = await this.cartRepository.findOne({ where: { user, id }, relations: ['products'] })
    if (!cart) throw new NotFoundException('Cart not found')
    return cart
  }
  
  addToCart(productId: number, user: User): Promise<Cart> {
    return this.cartRepository.addToCart(productId, user)
  }

  async updateCartStatus(id: number, user: User, status?: CartStatus): Promise<Cart> {
    const cart = await this.getCartById(id, user)
    if (status) cart.status = status
    else if (cart.status === CartStatus.OPEN) cart.status = CartStatus.COMPLETED
    else if (cart.status === CartStatus.COMPLETED) cart.status = CartStatus.CLOSED
    await cart.save()
    return cart
  }
}
