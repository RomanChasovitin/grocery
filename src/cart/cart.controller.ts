import { Controller, UseGuards, Post, Body, ParseIntPipe, Get, Param, Patch } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { User } from 'src/auth/user.entity';
import { CartService } from './cart.service';
import { Cart } from './cart.entity';
import { CartStatus } from './cart-status.enum';

@Controller('carts')
@UseGuards(AuthGuard('jwt'))
export class CartController {
  constructor(
    private cartService: CartService,
  ) {}
  
  @Get('/:id')
  getCartById(
    @Param('id', ParseIntPipe) id: number,
    @GetUser() user: User
  ): Promise<Cart> {
    return this.cartService.getCartById(id, user)
  }

  @Get()
  getAllCarts(
    @GetUser() user: User
  ): Promise<Cart[]> {
    return this.cartService.getAllCarts(user)
  }

  @Post('/add')
  addToCart(
    @Body('productId', ParseIntPipe) productId: number,
    @GetUser() user: User,
  ): Promise<Cart> {
    return this.cartService.addToCart(productId, user)
  }

  @Patch('/:id')
  updateCartStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status') status: CartStatus,
    @GetUser() user: User,
  ): Promise<Cart> {
    return this.cartService.updateCartStatus(id, user, status)
  }
}
