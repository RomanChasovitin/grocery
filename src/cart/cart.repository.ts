import { Repository, EntityRepository } from "typeorm"
import { Cart } from "./cart.entity";
import { Product } from "src/product/product.entity";
import { CartStatus } from "./cart-status.enum";
import { NotFoundException, BadRequestException } from "@nestjs/common";
import { User } from "src/auth/user.entity";

@EntityRepository(Cart)
export class CartRepository extends Repository<Cart> {
  async addToCart(productId: number, user: User): Promise<Cart> {
    const product = await Product.findOne({ id: productId })
    if (!product) throw new NotFoundException('Product not found')

    let cart = await this.findOne({ status: CartStatus.OPEN, user }, { relations: ['products'] })
    if (!cart) cart = await this.createCart(user)

    const exist = cart.products.findIndex(p => p.id === product.id) + 1
    if (exist) throw new BadRequestException('Product already exist in this cart')
    
    cart.products = [...cart.products, product]
    await cart.save()
    return cart
  }

  private async createCart(user: User) {
    const cart = new Cart()
    cart.user = user
    cart.products = []
    await cart.save()
    return cart
  }
}