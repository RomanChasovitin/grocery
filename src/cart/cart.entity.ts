import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import { CartStatus } from "./cart-status.enum";
import { User } from "src/auth/user.entity";
import { Product } from "src/product/product.entity";

@Entity()
export class Cart extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: CartStatus.OPEN })
  status: CartStatus;

  @Column({ nullable: true })
  name: string;

  @ManyToOne(type => User, user => user.carts)
  user: User;

  @ManyToMany(type => Product)
  @JoinTable()
  products: Product[];
}