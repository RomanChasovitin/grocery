import { Controller, Post, Body, ValidationPipe, UseGuards, Get, Put } from '@nestjs/common';
import { AuthDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/signin.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './decorators/get-user.decorator';
import { User } from './user.entity';
import { UpdateUserDto } from './dto/update.dto'

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
  ) {}

  @Post('/signup')
  signUp(@Body(ValidationPipe) authDto: AuthDto) {
    return this.authService.signUp(authDto)
  }

  @Post('/signin')
  signIn(@Body(ValidationPipe) signInDto: SignInDto): Promise<{ token: string }> {
    return this.authService.signIn(signInDto)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/profile')
  getProfile(@GetUser() user: User): User {
    return this.authService.getProfile(user)
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/profile')
  updateProfile(
    @Body(ValidationPipe) updateUserDto: UpdateUserDto,
    @GetUser() user: User
  ): Promise<User> {
    return this.authService.updateProfile(updateUserDto, user)
  }
}
