import { Repository, EntityRepository } from "typeorm"
import { ConflictException, InternalServerErrorException } from "@nestjs/common"
import * as bcrypt from 'bcrypt'

import { User } from "./user.entity"
import { AuthDto } from "./dto/auth.dto"
import { SignInDto } from "./dto/signin.dto"

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(authDto: AuthDto): Promise<void> {
    const { username, password, name, gender } = authDto
    const user = new User()
    user.username = username
    user.salt = await bcrypt.genSalt()
    user.password = await this.hashPassword(password, user.salt)
    user.name = name
    user.gender = gender
    try {
      await user.save()
    } catch(error) {
      // 23505 = duplicate entities(username)
      if (error.code === '23505') throw new ConflictException('Username already exist')
      throw new InternalServerErrorException(error)
    }
  }

  async validateUserPassword(signInDto: SignInDto): Promise<string> {
    const { username, password } = signInDto
    const user = await this.findOne({ username })
    return user && await user.validatePassword(password) ? user.username : null
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt)
  }
}