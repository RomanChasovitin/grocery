import { IsString, MinLength, MaxLength, IsNotEmpty } from "class-validator";
import { Gender } from "src/auth/gender.enum";

export class UpdateUserDto {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  name: string;

  @IsNotEmpty()
  gender: Gender;
}