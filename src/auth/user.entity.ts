import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import * as bcrypt from 'bcrypt'
import { Gender } from './gender.enum'
import { Cart } from "src/cart/cart.entity";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  gender: Gender;

  @OneToMany(type => Cart, cart => cart.user)
  carts: Cart[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt)
    return hash === this.password
  }
}