import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';

import { UserRepository } from './user.repository';
import { AuthDto } from './dto/auth.dto';
import { JwtPayload } from './jwt/payload.interface';
import { SignInDto } from './dto/signin.dto';
import { User } from './user.entity';
import { UpdateUserDto } from './dto/update.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}
  
  async signUp(authDto: AuthDto): Promise<void> {
    return this.userRepository.signUp(authDto)
  }

  async signIn(signInDto: SignInDto): Promise<{ token: string }> {
    const username = await this.userRepository.validateUserPassword(signInDto)
    if (!username) throw new UnauthorizedException('Invalid credentials')

    const payload: JwtPayload = { username }
    const token = await this.jwtService.sign(payload)
    return { token }
  }

  getProfile(user: User): User {
    delete user.salt
    delete user.password
    return user
  }

  async updateProfile(updateUserDto: UpdateUserDto, user: User): Promise<User> {
    const { name, gender } = updateUserDto
    user.name = name
    user.gender = gender
    await user.save()
    return this.getProfile(user)
  }
}
