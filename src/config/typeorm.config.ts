import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { User } from 'src/auth/user.entity'
import { Product } from 'src/product/product.entity'
import { Cart } from 'src/cart/cart.entity'

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: '0550403078',
  database: 'grocery2',
  entities: [User, Product, Cart],
  synchronize: true,
}