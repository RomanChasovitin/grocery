// eslint-disable
const axios = require('axios')
const groceries = require('./groceries.json')
// TODO: Need to write correct logic
console.log(groceries.length)

async function getImages(products) {
  const results = await products.map(async product => {
    const image = await getImage(product.name)
    // await createProduct(product.name, image)
  })
}

async function getImage(name) {
  const API_URL = 'https://www.googleapis.com/customsearch/v1'
  const API_CX = '011607191737784066009:rguvge3u3jq'
  const API_KEY = 'AIzaSyA1YRxVoKPmI31aXUPvst6TQkDlzIYzOvY'

  try {
    const response = await axios.get(API_URL, {
      params: {
        searchType: 'image',
        q: name.replace(' ', '+'),
        key: API_KEY,
        cx: API_CX,
        num: 1,
        start: 1
      }
    })
    const result = response.data.items[0]
    return result.link
  } catch (err) {
    console.log(err.response.data)
  }
}

async function createProduct(name, image) {
  const auth = await axios.post('http://localhost:9999/auth/signin', {
    username: 'roman',
    password: '0550403078Roma99',
  })
  const {token} = auth.data
  axios.post('http://localhost:9999/products',
    { name, image },
    { 'Authorization': `Bearer ${token}` },
  ).then(() => console.log('Successfully upload product: ', name)).catch(err => console.warn(err))
}